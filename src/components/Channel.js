import React, { useEffect, useState, useRef } from "react";
import PropTypes from "prop-types";
import { useAuthState } from "react-firebase-hooks/auth";
import {
  collection,
  query,
  orderBy,
  limit,
  onSnapshot,
  serverTimestamp,
  addDoc,
} from "firebase/firestore";

import Message from "./Message";

import cn from "./Channel.module.scss";

const Channel = ({ db, auth }) => {
  const [user, loading, error] = useAuthState(auth);
  const messagesRef = collection(db, "messages");
  const [anonModeName, setAnonModeName] = useState(undefined);

  const messages = QueryFirestore(
    query(messagesRef, orderBy("createdAt", "desc"), limit(100))
  );

  const [newMessage, setNewMessage] = useState("");

  const inputRef = useRef();
  const bottomListRef = useRef();

  const { uid, displayName, photoURL } = user;

  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
  }, [inputRef]);

  const handleOnChange = (e) => {
    setNewMessage(e.target.value);
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();

    const trimmedMessage = newMessage.trim();
    if (trimmedMessage) {
      const obj = !anonModeName
        ? {
            text: trimmedMessage,
            createdAt: serverTimestamp(),
            uid,
            displayName,
            photoURL,
          }
        : {
            text: trimmedMessage,
            createdAt: serverTimestamp(),
            displayName: anonModeName,
          };
      addDoc(messagesRef, obj);
      setNewMessage("");
      bottomListRef.current.scrollIntoView({ behavior: "smooth" });
    }
  };

  if (error || loading) {
    return <Status loading={loading} error={error} />;
  }
  return (
    <div className={cn.root}>
      <div className={cn.messages}>
        <div className={cn.container}>
          <div className={cn.greet}>
            <Greet />
          </div>
          <Messages messages={messages} />
          <div ref={bottomListRef} />
        </div>
      </div>
      <div className={cn.root2}>
        <form onSubmit={handleOnSubmit} className={cn.form}>
          <input
            ref={inputRef}
            type="text"
            value={newMessage}
            onChange={handleOnChange}
            placeholder="Type your message here..."
            className={cn.input}
          />
          <button type="submit" disabled={!newMessage} className={cn.submit}>
            Send
          </button>
        </form>
        <Options
          anonModeName={anonModeName}
          setAnonModeName={setAnonModeName}
        />
      </div>
    </div>
  );
};

Channel.propTypes = {
  user: PropTypes.shape({
    uid: PropTypes.string,
    displayName: PropTypes.string,
    photoURL: PropTypes.string,
  }),
};

export default Channel;

export const QueryFirestore = (query) => {
  //test
  const queryRef = useRef(query);
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    const unsubscribe = onSnapshot(queryRef.current, (querySnapshot) => {
      const mess = [];
      querySnapshot.forEach((doc) => {
        mess.push(doc.data());
      });
      setMessages(mess);
    });
    return unsubscribe;
  }, [queryRef]);

  return messages;
};

export const Options = ({ anonModeName, setAnonModeName }) => {
  const [name, setName] = useState();
  const handleOnChange = (e) => {
    setName(e.target.value);
  };
  const handleOnSubmit = (e) => {
    e.preventDefault();
    setAnonModeName(name);
  };
  const hadnleClick = () => {
    setAnonModeName(undefined);
    setName("");
  };
  return (
    <>
      <div data-testid="options">
        <form onSubmit={handleOnSubmit} className={cn.options}>
          <input
            type="text"
            value={name}
            onChange={handleOnChange}
            placeholder="Name for anon mode"
            className={cn.input}
            data-testid="input"
          />
          <div className={cn.btns}>
            <button
              type="submit"
              disabled={!name}
              className={cn[`anonBtn${anonModeName ? "--active" : ""}`]}
            >
              Anonymous mode {!anonModeName ? " - disabled" : " - enabled"}
            </button>
            {anonModeName && (
              <button
                onClick={hadnleClick}
                style={{ color: "red" }}
                data-testid="cancel"
              >
                X
              </button>
            )}
          </div>
        </form>
      </div>
    </>
  );
};

// export const AuthState = (auth) => {
//   return useAuthState(auth);
// };

export const Messages = ({ messages }) => {
  return (
    <ul style={{ listStyle: "none" }} data-testid="messages">
      {messages
        ?.sort((first, second) =>
          first?.createdAt?.seconds <= second?.createdAt?.seconds ? -1 : 1
        )
        ?.map((message) => (
          <li key={message.id}>
            <Message {...message} />
          </li>
        ))}
    </ul>
  );
};

export const Greet = () => {
  return (
    <>
      <div className={cn["greet--title"]} data-testid="Greet">
        <p style={{ marginBottom: "0.25rem" }}>Welcome to</p>
        <p style={{ marginBottom: "0.75rem" }}>React FireChat</p>
      </div>
      <p className={cn["greet--text"]}>This is the beginning of this chat.</p>
    </>
  );
};

export const Status = ({ loading, error }) => {
  return (
    <>
      <div data-testid="Error">
        <p data-testid="Error-text">
          {error ? "something went wrong, try again" : "loading..."}
        </p>
      </div>
    </>
  );
};
