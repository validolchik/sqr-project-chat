import React, { useState } from "react";
import { act } from "react-dom/test-utils";
import renderer from "react-test-renderer";
import {
  render,
  cleanup,
  screen,
  waitFor,
  fireEvent,
} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import * as firebaseFunction from "react-firebase-hooks/auth";

import Channel, * as ChannelFunc from "../Channel";
// import { useState } from "react";

afterEach(cleanup);

// it('test', () => {
// 	render(<Channel />)
// })

it("options ", () => {
  render(<ChannelFunc.Options anonModeName="" setAnonModeName={undefined} />);
});

it("it renders options", () => {
  const { getByTestId } = render(
    <ChannelFunc.Options anonModeName="" setAnonModeName={undefined} />
  );
  expect(getByTestId("options")).not.toBeNull();
});

it("options cancel", async () => {
  //   const [name, setName] = useState("name");
  const { getByTestId } = render(
    <ChannelFunc.Options
      anonModeName={"name"}
      setAnonModeName={() => undefined}
    />
  );
  expect(getByTestId("cancel")).not.toBeNull();
  userEvent.click(getByTestId("cancel"));
  await waitFor(() =>
    expect(screen.getByTestId("input")).toHaveTextContent("")
  );
});

it("options handle", async () => {
  //   const [name, setName] = useState("name");
  const { getByTestId } = render(
    <ChannelFunc.Options
      anonModeName={"name"}
      setAnonModeName={() => undefined}
    />
  );
  expect(getByTestId("input")).not.toBeNull();
  const input = getByTestId("input");
  fireEvent.change(input, { target: { value: "new" } });
  expect(input.value).toBe("new");
});

it("messages ", () => {
  const { getByTestId } = render(
    <ChannelFunc.Messages
      messages={[
        {
          createdAt: {
            nanoseconds: 874000000,
            seconds: 1652009615,
          },
        },
        {
          createdAt: {
            nanoseconds: 874000000,
            seconds: 1652009615,
          },
        },
        {
          createdAt: {
            nanoseconds: 874000000,
            seconds: 1652009615,
          },
        },
      ]}
    />
  );
  expect(getByTestId("messages")).toBeInTheDocument();
});

it("Greet ", () => {
  const { getByTestId } = render(<ChannelFunc.Greet />);
  expect(getByTestId("Greet")).toBeInTheDocument();
});
it("Status  loading", () => {
  const { getByTestId } = render(
    <ChannelFunc.Status loading={true} error={false} />
  );
  expect(getByTestId("Error")).toBeInTheDocument();
  expect(getByTestId("Error-text")).toHaveTextContent("loading...");
});

it("Status  Error", () => {
  const { getByTestId } = render(
    <ChannelFunc.Status loading={false} error={true} />
  );
  expect(getByTestId("Error")).toBeInTheDocument();
  expect(getByTestId("Error-text")).toHaveTextContent(
    "something went wrong, try again"
  );
});

it("Options snapshot", () => {
  const tree = renderer.create(<ChannelFunc.Options />).toJSON();
  expect(tree).toMatchSnapshot();
});

// it("AuthState", () => {
//   const t = ChannelFunc.AuthState({ currentUser: "me" });
//   expect(t).ignoreUndefinedProperties();
// });

// test("spy", () => {
//   const spy = jest
//     .spyOn(firebaseFunction, "useAuthState")
//     .mockImplementation(() => [null, false, true]);

//   const { getByTestId } = render(<Channel />);
//   expect(getByTestId("error")).toBeInTheDocument();

//   spy.mockRestore();
// });
