# SQR Project - Chat app

## Context

Chat app is a project created by group of student during SQR course (Software Quality, Reliability, Security) in [Innopolis University](https://innopolis.university/). The aim of the project is to create simple web application with Authentication and chat room to communicate with other people using existing technologies (described below).

## Team

- Renat Valeev - r.valeev@innopolis.university
- Idel Ishbaev - i.ishbaev@innopolis.university
- Jameel Muhutdinov - D.muhutdinov@innopolis.university
- Tagir Shigapov - t.shigapov@innopolis.university
- Evgeniy Trantsev - e.trantsev@innopolis.university

## Project stack

- Frontend - React (JavaScript, Sass, HTML)
- CI/CD - GitLab actions, AWS
- Testing - pgTAP, Selenium, Jest, Enzyme, Quixote, JMeter, WFuzz

## Branching policy

Gitflow

<img width="400" alt="gitflow" src="https://user-images.githubusercontent.com/43718473/139717929-2c3c87a6-7dae-4a46-bfbd-11ac198f1029.png">

## Firebase

The project was developed with React and Firebase.

Firebase technologies was used to implement Auth (login and registration with email/pass, reset pass, login with Google).

Firestore databse was used to implement the chat itself.

## Development

clone the repo - [https://gitlab.com/validolchik/sqr-project-chat](https://gitlab.com/validolchik/sqr-project-chat)
using the comand below or by descktop applications

```bash
git clone git@gitlab.com:validolchik/sqr-project-chat.git
```

### Run locally

create `.env` file and put env variables. (to get them contact to one of the developers)

local: frontend (defaul address is [http://localhost:3000/](http://localhost:3000/))

```bash
cd frontend
npm i
npm run test
npm start
```

# Project status

The project is currently at development stage.
